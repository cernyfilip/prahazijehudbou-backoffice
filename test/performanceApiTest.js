import request from 'supertest'
import app from '../src'

const testApp = app

// ==================== performance API test data ====================

const completeMessage = {
  performance_start_date: 1547762571,
  performance_end_date: 1547762588
}

const uncompleteMessage = {
  performance_end_date: 1547762588
}

// ==================== performance API test ====================

/**
 * Testing get all performances endpoint
 */
describe('GET /v1/performance', () => {
  it('respond with json containing a list of all performances', (done) => {
    request(testApp)
      .get('/v1/performance')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200, done)
  })
  it('respond status 404 on nonexisting endpoint', (done) => {
    request(testApp)
      .get('/v1/performances')
      .expect(404, done)
  })
  it('respond with json containing a single performance detail', (done) => {
    request(testApp)
      .get('/v1/performance/5c327d4fe5baf360a8f55d8a')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200, done)
  })
  it('respond with 404 for nonexisting performances', (done) => {
    request(testApp)
      .get('/v1/performance/nonexistid')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(404, done)
  })
})
describe('POST /v1/performance/add', () => {
  it('respond with 201 for complete new performance message', (done) => {
    request(testApp)
      .post('/v1/performance/add/5c50525b3e6ad73852e5f400')
      .set('Accept', 'application/json')
      .send(completeMessage)
      .expect('Content-Type', /json/)
      .expect(201, done)
  })
  it('respond with 400 for uncomplete new performance message', (done) => {
    request(testApp)
      .post('/v1/performance/add/5c50525b3e6ad73852e5f400')
      .set('Accept', 'application/json/')
      .send(uncompleteMessage)
      .expect('Content-Type', /json/)
      .expect(400, done)
  })
  it('respond with 404 for non exist performer id new performance message', (done) => {
    request(testApp)
      .post('/v1/performance/add/5c50525b3e6ad73852e5f500')
      .set('Accept', 'application/json')
      .send(completeMessage)
      .expect('Content-Type', /json/)
      .expect(404, done)
  })
})
