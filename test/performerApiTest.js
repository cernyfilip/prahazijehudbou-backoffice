import request from 'supertest'
import app from '../src'

const testApp = app

// ==================== performer API test data ====================

const completeMessage = {
  created_on: '2018-07-03T15:15:40.000Z',
  your_subject: 'test subject',
  your_name: 'test name',
  your_phone: '777777777',
  your_email: 'test@email.cz',
  pocet_vystupujicich: '2',
  pa_rano: null,
  pa_odpo: null,
  pa_vecer: '1',
  pa_after: null,
  so_rano: null,
  so_odpo: null,
  so_vecer: '1',
  so_after: null,
  typ_vystoupeni: 'acrobatic performance',
  zanr: 'new circus',
  nazev_vystoupeni: 'performance name',
  kratky_popis_vystoupeni: 'short performance description',
  ukazka_na_youtube: 'https://www.youtube.com/watch?v=2O0k7e5ApFU',
  url_facebook: 'http://www.facebook.com',
  delka_vystoupeni: 60,
  specialni_pozadavky: null,
  ucinkovali: 'Ne (no)',
  img_url: 'https://imgur.com'
}

const uncompleteMessage = {
  so_vecer: '1',
  so_after: null,
  typ_vystoupeni: 'acrobatic performance',
  zanr: 'new circus',
  nazev_vystoupeni: 'performance name',
  kratky_popis_vystoupeni: 'short performance description',
  ukazka_na_youtube: 'https://www.youtube.com/watch?v=2O0k7e5ApFU',
  url_facebook: 'http://www.facebook.com',
  delka_vystoupeni: 60,
  specialni_pozadavky: null,
  ucinkovali: 'Ne (no)',
  img_url: 'https://imgur.com'
}

// ==================== performer API test ====================

/**
 * Testing get all performers endpoint
 */
describe('GET /v1/performer', () => {
  it('respond with json containing a list of all performers', (done) => {
    request(testApp)
      .get('/v1/performer')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200, done)
  })
  it('respond status 404 on nonexisting endpoint', (done) => {
    request(testApp)
      .get('/v1/performers')
      .expect(404, done)
  })
  it('respond with json containing a single performer detail', (done) => {
    request(testApp)
      .get('/v1/performer/5c327d4fe5baf360a8f55d8a')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200, done)
  })
  it('respond with 404 for nonexisting performers', (done) => {
    request(testApp)
      .get('/v1/performer/nonexistid')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(404, done)
  })
})
describe('POST /v1/performer/add', () => {
  it('respond with 201 for complete new performer message', (done) => {
    request(testApp)
      .post('/v1/performer/add')
      .set('Accept', 'application/json')
      .send(completeMessage)
      .expect('Content-Type', /json/)
      .expect(201, done)
  })
  it('respond with 400 for uncomplete new performer message', (done) => {
    request(testApp)
      .post('/v1/performer/add')
      .set('Accept', 'application/json')
      .send(uncompleteMessage)
      .expect('Content-Type', /json/)
      .expect(400, done)
  })
})
