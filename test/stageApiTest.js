import request from 'supertest'
import app from '../src'

const fs = require('fs')
const dummyjson = require('dummy-json')
const path = require('path')

const testApp = app

// ==================== stage API test data ====================

const completeMessageTemplate = fs.readFileSync(path.join(__dirname, '/messageTemplates/stageCompleteTemplate.hbs'), { encoding: 'utf8' })
const completeMessageParsed = dummyjson.parse(completeMessageTemplate)
const completeMessage = JSON.parse(completeMessageParsed)

const uncompleteMessageTemplate = fs.readFileSync(path.join(__dirname, '/messageTemplates/stageUncompleteTemplate.hbs'), { encoding: 'utf8' })
const uncompleteMessageParsed = dummyjson.parse(uncompleteMessageTemplate)
const uncompleteMessage = JSON.parse(uncompleteMessageParsed)

// ==================== stage API test ====================

/**
 * Testing get all stages endpoint
 */
describe('GET /v1/stage', () => {
  it('respond with json containing a list of all stages', (done) => {
    request(testApp)
      .get('/v1/stage')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200, done)
  })
  it('respond status 404 on nonexisting endpoint', (done) => {
    request(testApp)
      .get('/v1/stages')
      .expect(404, done)
  })
  it('respond with json containing a single stage detail', (done) => {
    request(testApp)
      .get('/v1/stage/5c327d4fe5baf360a8f55d8a')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200, done)
  })
  it('respond with 404 for nonexisting stages', (done) => {
    request(testApp)
      .get('/v1/stage/nonexistid')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(404, done)
  })
})
describe('POST /v1/stage/add', () => {
  it('respond with 201 for complete new stage message', (done) => {
    request(testApp)
      .post('/v1/stage/add')
      .set('Accept', 'application/json')
      .send(completeMessage)
      .expect('Content-Type', /json/)
      .expect(201, done)
  })
  it('respond with 400 for uncomplete new stage message', (done) => {
    request(testApp)
      .post('/v1/stage/add')
      .set('Accept', 'application/json')
      .send(uncompleteMessage)
      .expect('Content-Type', /json/)
      .expect(400, done)
  })
})
