import mongoose from 'mongoose'
import { Router } from 'express'
import Performer from '../model/performer'
import Stage from '../model/stage'
import Performance from '../model/performance'
import Volunteer from '../model/volunteer'

import { authenticate } from '../middleware/authMiddleware'

mongoose.Promise = global.Promise
export default ({ config, db }) => {
  const api = Router()

  // API Welcome message
  api.get('/', (req, res) => {
    res.json({ message: 'Welcome to Praha Zije Hudbou API' })
  })

  // Volunteer Endpoint CRUD - Create Read Update Delete

  // '/v1/volunteer/add' - Create
  api.post('/volunteer/add', (req, res) => {
    const newVolunteer = new Volunteer()
    newVolunteer.name = req.body.name
    newVolunteer.phone = req.body.phone
    newVolunteer.email = req.body.email
    newVolunteer.person_bio = req.body.person_bio
    newVolunteer.festival_expectation = req.body.festival_expectation
    newVolunteer.festival_experience = req.body.festival_experience
    newVolunteer.festival_motivation = req.body.festival_motivation
    newVolunteer.approved = req.body.approved,
    newVolunteer.deleted = req.body.deleted,
    newVolunteer.stage_id = req.body.stage_id
    newVolunteer.stage_name = req.body.stage_name

    newVolunteer.save((err) => {
      if (err) {
        return res
          .status(400)
          .json(err)
      }
      return res
        .status(200)
        .json({ message: 'Volunteer saved successfully' })
    })
  })

  api.get('/volunteer/', (req, res) => {
    Volunteer.find({}, (err, volunteers) => {
      if (err) {
        res.send(err)
      }
      res.json(volunteers)
    })
  })

  api.get('/volunteer/:id', (req, res) => {
    Volunteer.findById(req.params.id, (err, volunteer) => {
      if (err) {
        res
          .status(404)
          .send(err)
      }
      res.json(volunteer)
    })
  })

  api.put('/volunteer/:id', (req, res) => {
    Volunteer.findByIdAndUpdate(req.params.id, req.body, { new: true }, (err, volunteer) => {
      if (err) {
        return res
          .status(404)
          .send(err)
      }
      return res.send(volunteer)
    })
  })
  // Performer Endpoint CRUD - Create Read Update Delete

  // '/v1/performer/add' - Create
  api.post('/performer/add', (req, res) => {
    const newPerformer = new Performer()
    newPerformer.created_on = req.body.created_on
    newPerformer.performer_name = req.body.your_subject
    newPerformer.contact_name = req.body.your_name
    newPerformer.contact_phone = req.body.your_phone
    newPerformer.contact_email = req.body.your_email
    newPerformer.number_of_performers = req.body.pocet_vystupujicich
    newPerformer.friday_morning = req.body.pa_rano
    newPerformer.friday_afternoon = req.body.pa_odpo
    newPerformer.friday_evening = req.body.pa_vecer
    newPerformer.friday_after = req.body.pa_after
    newPerformer.saturday_morning = req.body.so_rano
    newPerformer.saturday_afternoon = req.body.so_odpo
    newPerformer.saturday_evening = req.body.so_vecer
    newPerformer.saturday_after = req.body.so_after
    newPerformer.type_of_performance = req.body.typ_vystoupeni
    newPerformer.genre = req.body.zanr
    newPerformer.title_of_performance = req.body.nazev_vystoupeni
    newPerformer.performance_description_cz = req.body.kratky_popis_vystoupeni_cz
    newPerformer.performance_description_en = req.body.kratky_popis_vystoupeni_en
    newPerformer.youtube_url = req.body.ukazka_na_youtube
    newPerformer.facebook_url = req.body.url_facebook
    newPerformer.performance_length = req.body.delka_vystoupeni
    newPerformer.special_demands = req.body.specialni_pozadavky
    newPerformer.performed = req.body.ucinkovali
    newPerformer.img_user = req.body.img_user
    newPerformer.approved = req.body.approved
    newPerformer.deleted = req.body.deleted

    newPerformer.save((err) => {
      if (err) {
        return res
          .status(400)
          .json({ error: 'Error occured, please fill out all required fields' })
      }
      return res
        .status(201)
        .json({ message: 'Performer saved successfully' })
    })
  })

  // '/v1/performer/' - Read
  api.get('/performer/', (req, res) => {
    Performer.find({}, (err, performers) => {
      if (err) {
        res.send(err)
      }
      res.json(performers)
    })
  })

  // '/v1/performer/:id' - Read 1
  api.get('/performer/:id', (req, res) => {
    Performer.findById(req.params.id, (err, performer) => {
      if (err) {
        res
          .status(404)
          .send(err)
      }
      res.json(performer)
    })
  })

  // '/v1/performer/:id' - Update 1
  api.put('/performer/:id', (req, res) => {
    Performer.findByIdAndUpdate(req.params.id, req.body, { new: true }, (err, performer) => {
      if (err) {
        return res
          .status(404)
          .send(err)
      }
      return res.send(performer)
    })
  })

  // Stage Endpoint CRUD - Create Read Update Delete

  // '/v1/stage/add' - Create
  api.post('/stage/add', (req, res) => {
    const newStage = new Stage()
    newStage.stage_name = req.body.stage_name
    newStage.stage_priority = req.body.stage_priority
    newStage.coordinates_lat = req.body.coordinates_lat
    newStage.coordinates_att = req.body.coordinates_att
    newStage.stage_opened_date = req.body.stage_opened_date
    newStage.stage_closed_date = req.body.stage_closed_date
    newStage.stage_main_img = req.body.stage_main_img
    newStage.stage_secondary_img = req.body.stage_secondary_img
    newStage.stage_custom_pin = req.body.stage_custom_pin
    newStage.stage_partner_logo = req.body.stage_partner_logo
    newStage.stage_detail_main_description = req.body.stage_detail_main_description
    newStage.stage_detail_secondary_description = req.body.stage_detail_secondary_description
    newStage.stage_button_main_text = req.body.stage_button_main_text
    newStage.stage_button_main_url = req.body.stage_button_main_url
    newStage.stage_button_secondary_text = req.body.stage_button_secondary_text
    newStage.stage_button_secondary_url = req.body.stage_button_secondary_url
    newStage.stage_main_color = req.body.stage_main_color
    newStage.stage_secondary_color = req.body.stage_secondary_color

    newStage.save((err) => {
      if (err) {
        return res
          .status(400)
          .send(err)
      }
      return res
        .status(201)
        .json({ message: 'Stage saved successfully' })
    })
  })

  // '/v1/stage/' - Read
  api.get('/stage/', (req, res) => {
    Stage.find({}, (err, stages) => {
      if (err) {
        res.send(err)
      }
      res.json(stages)
    })
  })

  // '/v1/stage/:id' - Read 1
  api.get('/stage/:id', (req, res) => {
    Stage.findById(req.params.id, (err, stage) => {
      if (err) {
        res
          .send(err)
      }
      res.json(stage)
    })
  })

  // '/v1/stage/:id' - Update 1
  api.put('/stage/:id', (req, res) => {
    Stage.findByIdAndUpdate(req.params.id, req.body, { new: true }, (err, stage) => {
      if (err) {
        return res
          .status(404)
          .send(err)
      }
      return res.send(stage)
    })
  })
  // Performance Endpoint CRUD - Create Read Update Delete

  // '/v1/performance/add' - Create
  api.post('/performance/add/:id', (req, res) => {
    Performer.findById(req.params.id, (err, performer) => {
      if (err || performer == null) {
        return res
          .status(404)
          .json({ error: 'Can\'t find performer with provided id' })
      }

      const newPerformance = new Performance()

      newPerformance.performer = performer._id
      newPerformance.performance_start_date = req.body.performance_start_date
      newPerformance.performance_end_date = req.body.performance_end_date
      newPerformance.performance_approved = req.body.performance_approved
      newPerformance.performance_name = req.body.perfromance_name
      newPerformance.stage_id = req.body.stage_id
      newPerformance.save((err, performer) => {
        if (err) {
          return res
            .status(400)
            .json(err)
        }
        performer.save((err) => {
          if (err) {
            return res
              .status(500)
              .json({ error: 'Something went wrong with server, please try again later' })
          }
          return res
            .status(201)
            .json({ message: 'Performance saved' })
        })
      })
    })
  })

  // '/v1/performance/' - Read
  api.get('/performance/', (req, res) => {
    Performance.find({}, (err, performances) => {
      if (err) {
        res.send(err)
      }
      res.json(performances)
    })
  })

  // '/v1/performance/:id' - Read 1
  api.get('/performance/:id', (req, res) => {
    Performance.findById(req.params.id, (err, performance) => {
      if (err) {
        res
          .status(404)
          .send(err)
      }
      res.json(performance)
    })
  })

  // '/v1/performance/:id' - Update 1
  api.put('performance/:id', (req, res) => {
    Performance.findByIdAndUpdate(req.params.id, req.body, { new: true }, (err, performance) => {
      if (err) {
        return res
          .status(404)
          .send(err)
      }
      return res
        .send(performance)
    })
  })
  return api
}
