import mongoose from 'mongoose'

const Schema = mongoose.Schema

let Volunteer = new Schema({
  name: {
    type: String
  },
  phone: {
    type: String
  },
  email: {
    type: String
  },
  person_bio: {
    type: String
  },
  festival_expectation: {
    type: String
  },
  festival_experience: {
    type: String
  },
  festival_motivation: {
    type: String
  },
  approved: {
    type: Boolean,
    default: false
  },
  deleted: {
    type: Boolean,
    default: false
  },
  stage_id: {
    type: Schema.Types.ObjectId,
    ref: 'Stage'
  },
  stage_name: {
    type: String
  }
})

module.exports = mongoose.model('Volunteer', Volunteer)
