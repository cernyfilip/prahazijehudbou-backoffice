import mongoose from 'mongoose'

mongoose.Promise = global.Promise
const Schema = mongoose.Schema

const stageSchema = new Schema({
  stage_name: {
    type: String
  },
  stage_priority: {
    type: String,
  },
  coordinates_lat: {
    type: String
  },
  coordinates_att: {
    type: String
  },
  stage_opened_date: {
    type: Date
  },
  stage_closed_date: {
    type: Date
  },
  performances: {
    type: [Schema.Types.ObjectId],
    ref: 'Performances'
  },
  stage_main_img: {
    type: String
  },
  stage_secondary_img: {
    type: String
  },
  stage_custom_pin: {
    type: String
  },
  stage_partner_logo: {
    type: String
  },
  stage_detail_main_description: {
    type: String
  },
  stage_detail_secondary_description: {
    type: String
  },
  stage_button_main_text: {
    type: String
  },
  stage_button_main_url: {
    type: String
  },
  stage_button_secondary_text: {
    type: String
  },
  stage_button_secondary_url: {
    type: String
  },
  stage_main_color: {
    type: String
  },
  stage_secondary_color: {
    type: String
  }
})

module.exports = mongoose.model('Stage', stageSchema)
