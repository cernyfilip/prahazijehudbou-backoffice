import mongoose from 'mongoose'

mongoose.Promise = global.Promise
const Schema = mongoose.Schema

const performanceSchema = new Schema({
  performer: {
    type: Schema.Types.ObjectId,
    ref: 'Performer'
  },
  performance_name: {
    type: String
  },
  performance_start_date: {
    type: String
  },
  performance_end_date: {
    type: String
  },
  performance_approved: {
    type: Boolean,
    default: false
  },
  stage_id: {
    type: Schema.Types.ObjectId,
    ref: 'Stage'
  }
})

module.exports = mongoose.model('Performance', performanceSchema)
