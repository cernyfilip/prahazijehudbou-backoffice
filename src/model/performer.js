import mongoose from 'mongoose'

mongoose.Promise = global.Promise
const Schema = mongoose.Schema

const performerSchema = new Schema({
  created_on: {
    type: String

  },
  performer_name: {
    type: String

  },
  contact_name: {
    type: String

  },
  contact_phone: {
    type: String
  },
  contact_email: {
    type: String
  },
  number_of_performers: {
    type: String
  },
  friday_morning: {
    type: String
  },
  friday_afternoon: {
    type: String
  },
  friday_evening: {
    type: String
  },
  friday_after: {
    type: String
  },
  saturday_morning: {
    type: String
  },
  saturday_afternoon: {
    type: String
  },
  saturday_evening: {
    type: String
  },
  saturday_after: {
    type: String
  },
  type_of_performance: {
    type: String
  },
  genre: {
    type: String
  },
  title_of_performance: {
    type: String
  },
  performance_description_cz: {
    type: String
  },
  performance_description_en: {
    type: String
  },
  youtube_url: {
    type: String
  },
  facebook_url: {
    type: String
  },
  performance_length: {
    type: Number
  },
  special_demands: {
    type: String
  },
  performed: {
    type: String
  },
  img_user: {
    type: String
  },
  approved: {
    type: Boolean,
    default: false
  },
  deleted: {
    type: Boolean,
    default: false
  }

})

module.exports = mongoose.model('Performer', performerSchema)
