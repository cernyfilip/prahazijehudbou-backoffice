import express from 'express'
import config from '../config'
import middleware from '../middleware'
import initializeDb from '../db'
import controller from '../controller/controller'
import user from '../controller/user'

const router = express()

// connect to db
initializeDb((db) => {
  // internal middleware
  router.use(middleware({ config, db }))

  // api routes v1 (/v1)
  router.use('/', controller({ config, db }))
  router.use('/user', user({ config, db }))
})

export default router
