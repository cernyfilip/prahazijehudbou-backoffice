import mongoose from 'mongoose'
require('dotenv').config()

export default (callback) => {
  const db = mongoose.connect(process.env.MONGO_URI, {
    useMongoClient: true
  })
  callback(db)
}
