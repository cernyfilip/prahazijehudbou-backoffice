import http from 'http'
import express from 'express'
import bodyParser from 'body-parser'
import mongoose from 'mongoose'
import passport from 'passport'
import routes from './routes'
import cors from 'cors'
import multer from 'multer'

const LocalStrategy = require('passport-local').Strategy

require('dotenv').config()

mongoose.Promise = global.Promise
const app = express()

app.server = http.createServer(app)

const port = process.env.PORT

// middleware
// parse application/json
app.use(bodyParser.json({
  type: 'application/json',
  limit: '5mb'
}))

// CORS middleware
app.use(cors({ credentials: true, origin: true }))

// passport config
app.use(passport.initialize())
let User = require('./model/user')
passport.use(new LocalStrategy({
  usernameField: 'email',
  passwordField: 'password'
},
User.authenticate()
))
passport.serializeUser(User.serializeUser())
passport.deserializeUser(User.deserializeUser())

// api routes v1
app.use('/v1', routes)
console.log(`Started on port ${port}`)

module.exports = app.server.listen(port)
